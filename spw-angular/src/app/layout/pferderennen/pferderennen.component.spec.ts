import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PferderennenComponent } from './pferderennen.component';

describe('PferderennenComponent', () => {
  let component: PferderennenComponent;
  let fixture: ComponentFixture<PferderennenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PferderennenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PferderennenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
