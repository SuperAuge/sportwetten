import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../session.service';
import {dateToGermanDateString} from '../../utility/DateConverter';
import {Wettschein} from "../../model/wettschein";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  columnsToDisplayFussballwetten = ['heimTeam', 'quoteHeim', 'quoteX', 'quoteGast', 'gastTeam', 'spielStart', 'spielEnde'];

  zuletztAktualisiertLaufend: Date;
  zuletztAktualisiertBeendet: Date;
  laufendeWettscheine = null;
  beendeteWettscheine = null;

  constructor(public sessionService: SessionService) {
  }

  ngOnInit(): void {
  }

  getWettscheine(action: string): Wettschein[] {

    /* Aktualisierung der Wettscheine alle 10 Sekunden: */
    if ('laufend' === action && this.laufendeWettscheine != null &&
      (this.zuletztAktualisiertLaufend.getTime() + 10 * 1000) > new Date().getTime()) {
      return this.laufendeWettscheine;
    }
    if ('beendet' === action && this.beendeteWettscheine != null &&
      (this.zuletztAktualisiertBeendet.getTime() + 10 * 1000) > new Date().getTime()) {
      return this.beendeteWettscheine;
    }
    /*  */

    const wettscheine = this.sessionService.user.wettscheine;
    const actionWettscheine = [];

    if (wettscheine == null || wettscheine.length === 0) {
      return [];
    }

    // Ist ein Spiel des Wettscheins noch am laufen, füge diesen zu den laufenden Wettscheinen hinzu
    for (const schein of wettscheine) {
      let beendeteSpiele = 0;
      let scheinActionBedingungErfuellt = false;

      for (const wette of schein.fussballwetten) {
        if ('laufend' === action) {
          scheinActionBedingungErfuellt = new Date(wette.spiel.spielStart).getTime() < new Date().valueOf() &&
            new Date().getTime() < wette.spiel.spielEnde.getTime();
        } else if ('beendet' === action) {
          if (wette.spiel.spielEnde.getTime() < new Date().getTime()) {
            beendeteSpiele++;
          }
        }

        if ('laufend' == action && scheinActionBedingungErfuellt ||
          'beendet' == action && beendeteSpiele == schein.fussballwetten.length) {
          actionWettscheine.push(schein);
          break;
        }
      }
    }

    if ('laufend' === action) {
      this.laufendeWettscheine = actionWettscheine;
      this.zuletztAktualisiertLaufend = new Date();
    } else if ('beendet' === action) {
      this.beendeteWettscheine = actionWettscheine;
      this.zuletztAktualisiertBeendet = new Date();
    }

    return actionWettscheine;
  }

  getDateString(date: Date) {
    return dateToGermanDateString(date);
  }
}
