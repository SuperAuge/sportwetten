import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../session.service';
import {LoginComponent} from '../popups/login/login.component';
import {MatDialog} from '@angular/material/dialog';
import {SnackbarService} from '../../utility/SnackbarService';
import {EinzahlenComponent} from '../popups/einzahlen/einzahlen.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  appTitle = 'Sportwetten';

  constructor(public sessionService: SessionService,
              private dialog: MatDialog,
              private snackbarService: SnackbarService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  onLoginClicked() {
    this.dialog.open(LoginComponent, {
      height: 'auto',
      width: '400px',
      autoFocus: false
    });
  }

  onLogoutClicked() {
    this.sessionService.user.benutzername = '';
    this.sessionService.user.passwort = '';
    this.sessionService.user.guthaben = 0;
    this.router.navigate(['']);
    this.snackbarService.showSuccessMessage('Sie wurden erfolgreich ausgeloggt.', 'OK.');
  }

  onEinzahlenClicked() {
    this.dialog.open(EinzahlenComponent, {
      height: 'auto',
      width: '400px',
      autoFocus: false
    });
  }
}
