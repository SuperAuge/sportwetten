import {Component, OnInit} from '@angular/core';
import {SessionService} from "../../session.service";
import {
  dateToGermanDateString,
  dateToGermanTimeString,
  differenceHourMinuteBetweenToDates
} from "../../utility/DateConverter";
import {Wettschein} from "../../model/wettschein";
import {FussballWette} from "../../model/fussballWette";
import {Wetteingabe} from "../../model/wetteingabe";
import {SnackbarService} from "../../utility/SnackbarService";
import {MatDialog} from "@angular/material/dialog";
import {EinsatzComponent} from "../popups/einsatz/einsatz.component";
import {RestService} from "../../rest.service";
import {GOWetteWettschein} from "../../model/rest/GOWetteWettschein";
import {GOUserWettschein} from "../../model/rest/GOUserWettschein";

@Component({
  selector: 'app-fussball',
  templateUrl: './fussball.component.html',
  styleUrls: ['./fussball.component.scss']
})
export class FussballComponent implements OnInit {

  quoteHover: string;
  ausgewaehlteQuote: string;

  ausgewaehlterWettschein: Wettschein;
  neuerWettschein: Wettschein = new Wettschein();

  constructor(public sessionService: SessionService,
              private snackbarService: SnackbarService,
              private dialog: MatDialog,
              private rest: RestService) {
  }

  ngOnInit(): void {

    if (this.sessionService.aktuellesSpiel == null) {
      this.rest.getZufallsFussballSpiel().subscribe(spiel => {
        this.sessionService.aktuellesSpiel = spiel;
      });
    }
  }

  getTurnierFromAktuellesSpiel() {

    if (this.sessionService.aktuellesSpiel == null || this.sessionService.aktuellesSpiel.heimTeam == null) {
      return '';
    }

    if (this.sessionService.aktuellesSpiel.heimTeam.liga == this.sessionService.aktuellesSpiel.gastTeam.liga) {
      return this.sessionService.aktuellesSpiel.heimTeam.liga;
    } else if (this.sessionService.aktuellesSpiel.heimTeam.powerFaktor >= 80 && this.sessionService.aktuellesSpiel.gastTeam.powerFaktor >= 80) {
      return 'Champions League';
    } else if (this.sessionService.aktuellesSpiel.heimTeam.powerFaktor < 80 && this.sessionService.aktuellesSpiel.heimTeam.powerFaktor >= 70 &&
      this.sessionService.aktuellesSpiel.gastTeam.powerFaktor < 80 && this.sessionService.aktuellesSpiel.gastTeam.powerFaktor >= 70) {
      return 'Europa League';
    } else {
      return 'Freundschaftsspiel';
    }
  }

  onTippAbgebenClicked() {

    if (this.ausgewaehlterWettschein == null) {
      this.snackbarService.showErrorMessage('Bitte wählen Sie einen Wettschein aus, um Ihren Tipp abzugeben.', 'OK');
      return;
    }

    const wette = new FussballWette();
    let quote = 0.0;
    wette.spiel = this.sessionService.aktuellesSpiel;
    switch (this.ausgewaehlteQuote) {
      case 'Heim':
        wette.tipp = Wetteingabe.HEIM;
        quote = wette.spiel.quoteHeim;
        break;
      case 'X':
        wette.tipp = Wetteingabe.X;
        quote = wette.spiel.quoteX;
        break;
      case 'Gast':
        wette.tipp = Wetteingabe.GAST;
        quote = wette.spiel.quoteGast;
    }

    if (this.ausgewaehlterWettschein.nummer == null || this.ausgewaehlterWettschein.nummer == 0) {
      const dialogRef = this.dialog.open(EinsatzComponent, {
        height: 'auto',
        width: '460px',
        autoFocus: false,
        data: {quote: quote}
      });

      dialogRef.afterClosed().subscribe(einsatz => {
        if (einsatz && einsatz > 0) {
          this.ausgewaehlterWettschein = new Wettschein();
          if (this.sessionService.user.wettscheine == null || this.sessionService.user.wettscheine.length == 0) {
            this.ausgewaehlterWettschein.nummer = 1;
          } else {
            this.ausgewaehlterWettschein.nummer = this.sessionService.user.wettscheine.length + 1;
          }
          this.ausgewaehlterWettschein.einsatz = einsatz;
          this.ausgewaehlterWettschein.moeglicherGewinn = einsatz * quote;

          if (this.ausgewaehlterWettschein.fussballwetten == null) {
            this.ausgewaehlterWettschein.fussballwetten = [];
          }
          this.ausgewaehlterWettschein.fussballwetten.push(wette);

          this.rest.addWettscheinToUser(new GOUserWettschein(this.sessionService.user, this.ausgewaehlterWettschein)).subscribe(wettschein => {
            this.ausgewaehlterWettschein = wettschein;
          })
        }
      })
    }

    const generationObject: GOWetteWettschein = new GOWetteWettschein();
    generationObject.wette = wette;
    generationObject.wettschein = this.ausgewaehlterWettschein;

    this.rest.addFussbalLWetteToWettschein(generationObject).subscribe(() => {

    })

    this.ausgewaehlterWettschein = null;
  }

  getDateString(date: Date) {
    return dateToGermanDateString(date);
  }

  getTimeString(date: Date) {
    return dateToGermanTimeString(date);
  }

  getDifferenceHourMinuteBetweenToDates(date1: Date, date2: Date) {
    date1 = new Date(date1);
    if (date2 == null) {
      date2 = new Date();
    }

    return differenceHourMinuteBetweenToDates(date1, date2);
  }
}
