import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FussballComponent } from './fussball.component';

describe('FussballComponent', () => {
  let component: FussballComponent;
  let fixture: ComponentFixture<FussballComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FussballComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FussballComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
