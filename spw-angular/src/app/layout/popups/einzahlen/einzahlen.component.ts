import {Component, OnInit} from '@angular/core';
import {RestService} from '../../../rest.service';
import {SessionService} from '../../../session.service';
import {SnackbarService} from '../../../utility/SnackbarService';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-einzahlen',
  templateUrl: './einzahlen.component.html',
  styleUrls: ['./einzahlen.component.scss']
})
export class EinzahlenComponent implements OnInit {

  einzahlbetrag = 0.0;

  constructor(public dialogRef: MatDialogRef<EinzahlenComponent>,
              public sessionService: SessionService,
              public snackbarService: SnackbarService,
              public rest: RestService) {
  }

  ngOnInit(): void {
  }

  onAcceptBetragClicked() {
    this.rest.geldEinzahlen(this.sessionService.user, this.einzahlbetrag).subscribe(user => {
      if (user != null) {
        this.sessionService.user = user;
        this.snackbarService.showSuccessMessage(`${this.einzahlbetrag}€ wurden auf ihr Konto überwiesen.`, 'OK');
        this.dialogRef.close();
      } else {
        this.snackbarService.showErrorMessage('Es gab Probleme beim Einzahlen des Betrags.', 'OK');
      }
    });
  }
}
