import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {SessionService} from "../../../session.service";
import {SnackbarService} from "../../../utility/SnackbarService";
import {RestService} from "../../../rest.service";

@Component({
  selector: 'app-einsatz',
  templateUrl: './einsatz.component.html',
  styleUrls: ['./einsatz.component.scss']
})
export class EinsatzComponent implements OnInit {

  einsatz: number = 0.0;
  moeglicherGewinn = 0;

  quote = 0.0;

  constructor(public dialogRef: MatDialogRef<EinsatzComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public sessionService: SessionService,
              public snackbarService: SnackbarService,
              public rest: RestService) {
  }

  ngOnInit(): void {
    this.quote = this.data.quote;
  }

  onAcceptEinsatzClicked() {
    if (this.einsatz > 0 && this.moeglicherGewinn > 0) {
      this.dialogRef.close(this.einsatz);
    }
  }

  onEinsatzChanged() {
    if (this.einsatz < 0) {
      this.einsatz = 0;
      return;
    }

    this.moeglicherGewinn = parseInt(String(this.einsatz)) * this.quote;

    if (Number.isNaN(this.moeglicherGewinn)) {
      this.moeglicherGewinn = 0;
    }
  }
}
