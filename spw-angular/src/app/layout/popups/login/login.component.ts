import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../../session.service';
import {MatDialogRef} from '@angular/material/dialog';
import {RestService} from '../../../rest.service';
import {SnackbarService} from '../../../utility/SnackbarService';
import {User} from '../../../model/user';
import {FussballTeam} from "../../../model/fussballTeam";
import {FussballSpiel} from "../../../model/fussballSpiel";
import * as moment from "moment";
import {FussballWette} from "../../../model/fussballWette";
import {Wetteingabe} from "../../../model/wetteingabe";
import {Wettschein} from "../../../model/wettschein";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  benutzername = '';
  passwort = '';

  constructor(public sessionService: SessionService,
              public rest: RestService,
              public dialogRef: MatDialogRef<LoginComponent>,
              private snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
  }

  onLoginClicked() {
    if (!this.checkBenutzernameGuidelines()) {
      return;
    }
    if (this.benutzername.length > 0 && this.passwort.length > 0) {
      const user = new User();
      user.benutzername = this.benutzername;
      user.passwort = this.passwort;
      this.rest.userAnmelden(user).subscribe(benutzer => {
        if (benutzer != null) {

          if (benutzer.passwort === this.passwort) {
            this.sessionService.user.benutzername = benutzer.benutzername;
            this.sessionService.user.guthaben = benutzer.guthaben;
            this.snackbarService.showSuccessMessage('Sie wurden erfolgreich angemeldet.', 'OK');
            this.dialogRef.close();
          } else {
            this.snackbarService.showErrorMessage('Das angegebene Passwort ist nicht korrekt.', 'OK');
          }
        } else {
          this.snackbarService.showErrorMessage('Es konnte kein Benutzer zu den angegebenen Daten gefunden werden.', 'OK');
        }

        // Test-Daten
        const team1 = new FussballTeam();
        team1.name = 'Borussia Mönchengladbach';
        team1.liga = '1. Bundesliga';
        team1.powerFaktor = 80;
        const team2 = new FussballTeam();
        team2.name = 'Bayern München';
        team2.liga = '1. Bundesliga';
        team2.powerFaktor = 95;
        const team3 = new FussballTeam();
        team3.name = 'Real Madrid';
        team3.liga = 'La Liga';
        team3.powerFaktor = 97;
        const team4 = new FussballTeam();
        team4.name = 'Darmstadt';
        team4.liga = '2. Bundesliga';
        team4.powerFaktor = 58;

        const spiel1 = new FussballSpiel();
        spiel1.spielStart = moment(new Date()).subtract(48, "minutes").toDate();
        spiel1.spielEnde = new Date();
        spiel1.toreHeim = 4;
        spiel1.toreGast = 2;
        spiel1.heimTeam = team1;
        spiel1.gastTeam = team2;
        spiel1.quoteHeim = 3.0;
        spiel1.quoteGast = 1.8;
        spiel1.quoteX = 2.2;
        const spiel2 = new FussballSpiel();
        spiel2.spielStart = moment(new Date()).subtract(48, "minutes").toDate();
        spiel2.spielEnde = new Date();
        spiel2.toreHeim = 1;
        spiel2.toreGast = 1;
        spiel2.heimTeam = team3;
        spiel2.gastTeam = team4;
        spiel2.quoteHeim = 3.0;
        spiel2.quoteGast = 1.8;
        spiel2.quoteX = 2.1;
        const spiel3 = new FussballSpiel();
        spiel3.spielStart = moment(new Date()).subtract(48, "minutes").toDate()
        spiel3.spielEnde = moment(new Date()).add(42, "minutes").toDate();
        spiel3.toreHeim = 2;
        spiel3.toreGast = 2;
        spiel3.heimTeam = team2;
        spiel3.gastTeam = team3;
        spiel3.quoteHeim = 3.0;
        spiel3.quoteX = 2.3;
        spiel3.quoteGast = 1.8;

        const fussballWette1 = new FussballWette();
        fussballWette1.tipp = Wetteingabe.HEIM;
        fussballWette1.spiel = spiel1;
        const fussballWette2 = new FussballWette();
        fussballWette2.tipp = Wetteingabe.GAST;
        fussballWette2.spiel = spiel2;
        const fussballWette3 = new FussballWette();
        fussballWette3.tipp = Wetteingabe.X;
        fussballWette3.spiel = spiel3;

        const wettschein1 = new Wettschein();
        wettschein1.nummer = 1;
        wettschein1.einsatz = 15;
        wettschein1.moeglicherGewinn = 150;
        wettschein1.fussballwetten = [];
        wettschein1.fussballwetten.push(fussballWette1, fussballWette2, fussballWette3);
        const wettschein2 = new Wettschein();
        wettschein2.nummer = 2;
        wettschein2.einsatz = 20;
        wettschein2.moeglicherGewinn = 180;
        wettschein2.fussballwetten = [];
        wettschein2.fussballwetten.push(fussballWette1, fussballWette3);

        this.sessionService.user.wettscheine = [];
        this.sessionService.user.wettscheine.push(wettschein1);
        this.sessionService.user.wettscheine.push(wettschein2);
      });

    } else {
      this.benutzername = '';
      this.passwort = '';
      this.snackbarService.showErrorMessage('Bitte füllen Sie alle Felder aus.', 'OK');
    }
  }

  private checkBenutzernameGuidelines() {
    if (this.benutzername.indexOf(' ') > -1) {
      this.snackbarService.showErrorMessage('Der Benutzernamen darf keine Leerzeichen enthalten.', 'OK');
      return false;
    }

    return true;
  }

  onRegistrierenClicked() {
    if (!this.checkBenutzernameGuidelines()) {
      return;
    }
    const registriereUser = new User();
    registriereUser.benutzername = this.benutzername;
    registriereUser.passwort = this.passwort;

    this.rest.userRegistrieren(registriereUser).subscribe(statuscode => {
      if (statuscode === 0) {
        this.snackbarService.showSuccessMessage('Der User wurde erfolgreich registriert.', 'OK');
        this.rest.userAnmelden(registriereUser).subscribe(benutzer => {
          this.sessionService.user = benutzer;
          this.dialogRef.close();
        });
      } else if (statuscode === 1) {
        this.snackbarService.showErrorMessage('Es gab Probleme beim Zugriff auf die Datenbank.', 'OK');
      } else if (statuscode === 2) {
        this.snackbarService.showErrorMessage('Der angegebene Benutzername existiert bereits.', 'OK');
      }
    });
  }
}
