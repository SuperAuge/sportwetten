import * as moment from "moment";

export function stringToDate(ddMMyyyy: string) {
  return moment(ddMMyyyy, 'DD.MM.YYYY')
}

export function dateToGermanDateString(date: Date) {
  return moment(date).format('DD.MM.YYYY');
}

export function dateToGermanTimeString(date: Date) {
  return moment(date).get("hours") + ':'
    + (moment(date).get("minutes") > 10 ? moment(date).get("minutes") : '0' + moment(date).get("minutes")) + ':'
    + (moment(date).get("seconds") > 10 ? moment(date).get("seconds") : '0' + moment(date).get("seconds"));
}

export function differenceHourMinuteBetweenToDates(date1: Date, date2: Date) {
  let diff = date1.valueOf() - date2.valueOf();
  diff /= 60000;
  if (diff < 0) {
    diff *= (-1);
  }
  diff = Math.round(diff);
  let hours = 0;
  let minutes: number;
  if (diff >= 60) {
    hours = Math.round(diff / 60);
    minutes = diff - (hours * 60);
  } else {
    minutes = diff;
  }

  return hours > 0 ? hours + ' std ' + minutes + ' min' : minutes + ' min';
}
