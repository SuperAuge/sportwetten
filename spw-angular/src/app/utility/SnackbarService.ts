import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import {Injectable} from '@angular/core';

@Injectable()
export class SnackbarService {

  constructor(private snackbar: MatSnackBar) {
  }

  public showErrorMessage(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.panelClass = ['snackbarError'];
    config.duration = 3000;
    this.snackbar.open(message, action, config);
  }

  public showSuccessMessage(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.panelClass = ['snackbarSuccess'];
    config.duration = 3000;
    this.snackbar.open(message, action, config);
  }
}
