import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from './model/user';
import {GOWetteWettschein} from "./model/rest/GOWetteWettschein";
import {FussballSpiel} from "./model/fussballSpiel";
import {Wettschein} from "./model/wettschein";
import {GOUserWettschein} from "./model/rest/GOUserWettschein";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  private url = environment.baseUrl + '/rest';

  constructor(private http: HttpClient) {
  }

  userAnmelden(user: User): Observable<User> {
    console.log(`POST ${this.url}/checklogindata`);
    return this.http.post<User>(`${this.url}/checklogindata`, user);
  }

  userRegistrieren(user: User): Observable<number> {
    console.log(`PUT ${this.url}/registriereuser`);
    return this.http.put<number>(`${this.url}/registriereuser`, user);
  }

  geldEinzahlen(user: User, betrag: number): Observable<User> {
    console.log(`PUT ${this.url}/einzahlen/${betrag}`);
    return this.http.put<User>(`${this.url}/einzahlen/${betrag}`, user);
  }

  getZufallsFussballSpiel() {
    console.log(`GET ${this.url}/zufallsspiel`);
    return this.http.get<FussballSpiel>(`${this.url}/zufallsspiel`);
  }

  addFussbalLWetteToWettschein(goWetteWettschein: GOWetteWettschein) {
    console.log(`PUT ${this.url}/addFussballWetteToWettschein`);
    return this.http.put(`${this.url}/addFussballWetteToWettschein`, goWetteWettschein);
  }

  addWettscheinToUser(goUserWettschein: GOUserWettschein): Observable<Wettschein> {
    console.log(`PUT ${this.url}/addWettscheinToUser`);
    return this.http.put<Wettschein>(`${this.url}/addWettscheinToUser`, goUserWettschein);
  }
}
