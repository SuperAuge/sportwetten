import {FussballTeam} from './fussballTeam';

export class FussballSpiel {

  spielStart: Date;
  spielEnde: Date;
  heimTeam: FussballTeam;
  gastTeam: FussballTeam;
  toreHeim: number;
  toreGast: number;
  quoteHeim: number;
  quoteGast: number;
  quoteX: number;

}
