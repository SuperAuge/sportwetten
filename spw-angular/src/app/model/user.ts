import {FussballWette} from "./fussballWette";
import {Wettschein} from "./wettschein";


export class User {

  benutzername: string;
  passwort: string;
  guthaben: number;
  wettscheine: Wettschein[];
  registrationDate: Date;
  lastLogin: Date;

  constructor() {
  }
}
