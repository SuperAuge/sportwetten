import {FussballWette} from "./fussballWette";

export class Wettschein {

  nummer: number;
  fussballwetten: FussballWette[];
  einsatz: number;
  moeglicherGewinn: number;

}
