import {FussballSpiel} from "./fussballSpiel";
import {Wetteingabe} from "./wetteingabe";

export class FussballWette {

  spiel: FussballSpiel;
  tipp: Wetteingabe;

}
