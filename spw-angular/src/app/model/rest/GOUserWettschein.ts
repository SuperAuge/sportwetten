import {User} from "../user";
import {Wettschein} from "../wettschein";

export class GOUserWettschein {

  user: User;
  wettschein: Wettschein;

  constructor(user: User, wettschein: Wettschein) {
    this.user = user;
    this.wettschein = wettschein;
  }
}
