import {Injectable} from '@angular/core';
import {User} from './model/user';
import {FussballSpiel} from './model/fussballSpiel';
import {RestService} from "./rest.service";

@Injectable()
export class SessionService {

  user: User;
  aktuellesSpiel: FussballSpiel = new FussballSpiel();

  constructor(private rest: RestService) {
    this.user = new User();

    if (this.aktuellesSpiel == null || this.aktuellesSpiel.heimTeam == null) {
      this.rest.getZufallsFussballSpiel().subscribe(spiel => {
        this.aktuellesSpiel = spiel;
      });
    }

    // const team1 = new FussballTeam();
    // team1.name = 'Borussia Mönchengladbach';
    // team1.liga = '1. Bundesliga';
    // team1.powerFaktor = 80;
    // const team2 = new FussballTeam();
    // team2.name = 'Bayern München';
    // team2.liga = '1. Bundesliga';
    // team2.powerFaktor = 95;
    // const team3 = new FussballTeam();
    // team3.name = 'Real Madrid';
    // team3.liga = 'La Liga';
    // team3.powerFaktor = 97;
    // const team4 = new FussballTeam();
    // team4.name = 'Darmstadt';
    // team4.liga = '2. Bundesliga';
    // team4.powerFaktor = 58;
    //
    // const spiel1 = new FussballSpiel();
    // spiel1.spielStart = moment(new Date()).subtract(48, "minutes").toDate();
    // spiel1.spielEnde = new Date();
    // spiel1.toreHeim = 4;
    // spiel1.toreGast = 2;
    // spiel1.heimTeam = team1;
    // spiel1.gastTeam = team2;
    // spiel1.quoteHeim = 3.0;
    // spiel1.quoteGast = 1.8;
    // spiel1.quoteX = 2.2;
    // const spiel2 = new FussballSpiel();
    // spiel2.spielStart = moment(new Date()).subtract(48, "minutes").toDate();
    // spiel2.spielEnde = new Date();
    // spiel2.toreHeim = 1;
    // spiel2.toreGast = 1;
    // spiel2.heimTeam = team3;
    // spiel2.gastTeam = team4;
    // spiel2.quoteHeim = 3.0;
    // spiel2.quoteGast = 1.8;
    // spiel2.quoteX = 2.1;
    // const spiel3 = new FussballSpiel();
    // spiel3.spielStart = moment(new Date()).subtract(48, "minutes").toDate()
    // spiel3.spielEnde = moment(new Date()).add(42, "minutes").toDate();
    // spiel3.toreHeim = 2;
    // spiel3.toreGast = 2;
    // spiel3.heimTeam = team2;
    // spiel3.gastTeam = team3;
    // spiel3.quoteHeim = 3.0;
    // spiel3.quoteX = 2.3;
    // spiel3.quoteGast = 1.8;
    //
    // const fussballWette1 = new FussballWette();
    // fussballWette1.tipp = Wetteingabe.HEIM;
    // fussballWette1.spiel = spiel1;
    // const fussballWette2 = new FussballWette();
    // fussballWette2.tipp = Wetteingabe.GAST;
    // fussballWette2.spiel = spiel2;
    // const fussballWette3 = new FussballWette();
    // fussballWette3.tipp = Wetteingabe.X;
    // fussballWette3.spiel = spiel3;
    //
    // const wettschein1 = new Wettschein();
    // wettschein1.nummer = 1;
    // wettschein1.einsatz = 15;
    // wettschein1.moeglicherGewinn = 150;
    // wettschein1.fussballwetten = [];
    // wettschein1.fussballwetten.push(fussballWette1, fussballWette2, fussballWette3);
    // const wettschein2 = new Wettschein();
    // wettschein2.nummer = 2;
    // wettschein2.einsatz = 20;
    // wettschein2.moeglicherGewinn = 180;
    // wettschein2.fussballwetten = [];
    // wettschein2.fussballwetten.push(fussballWette1, fussballWette2, fussballWette3);
  }
}
