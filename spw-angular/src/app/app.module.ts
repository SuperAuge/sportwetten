import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavComponent} from './layout/nav/nav.component';
import {HomeComponent} from './layout/home/home.component';
import {FussballComponent} from './layout/fussball/fussball.component';
import {PferderennenComponent} from './layout/pferderennen/pferderennen.component';
import {SessionService} from './session.service';
import {HttpClientModule} from '@angular/common/http';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {LoginComponent} from './layout/popups/login/login.component';
import {MatDialogModule} from '@angular/material/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatListModule} from '@angular/material/list';
import {FormsModule} from '@angular/forms';
import {SnackbarService} from './utility/SnackbarService';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatExpansionModule} from '@angular/material/expansion';
import {EinzahlenComponent} from './layout/popups/einzahlen/einzahlen.component';
import {EinsatzComponent} from './layout/popups/einsatz/einsatz.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    FussballComponent,
    PferderennenComponent,
    LoginComponent,
    EinzahlenComponent,
    EinsatzComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatListModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatDialogModule,
    MatSnackBarModule,
    HttpClientModule,
    FormsModule,
    MatTabsModule,
    MatTableModule,
    MatExpansionModule,
  ],
  providers: [
    SessionService,
    SnackbarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
