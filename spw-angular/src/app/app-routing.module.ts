import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './layout/home/home.component';
import {FussballComponent} from './layout/fussball/fussball.component';
import {PferderennenComponent} from './layout/pferderennen/pferderennen.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'fussball', component: FussballComponent
  },
  {
    path: 'pferderennen', component: PferderennenComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
