package de.dsa.spw;

public class MainTest {
    public static void main(String[] args) {
        Application.main("--spring.profiles.active=dev");
    }
}
