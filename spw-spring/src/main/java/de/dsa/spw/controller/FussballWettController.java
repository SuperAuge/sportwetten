package de.dsa.spw.controller;

import de.dsa.spw.model.FussballSpiel;
import de.dsa.spw.model.FussballTeam;
import de.dsa.spw.services.SQLiteService;
import de.dsa.spw.util.DateUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Random;

public class FussballWettController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FussballWettController.class);

    private List<FussballTeam> fussballTeams;
    private final SQLiteService sqLiteService;

    public FussballWettController() {
        this.sqLiteService = new SQLiteService();
        this.fussballTeams = this.sqLiteService.getFussballTeams();

        LOGGER.info("Zufaelliges FussballSpiel: {}", createRandomFussballSpiel(new Date()));
    }

    public FussballSpiel createRandomFussballSpiel(Date spielstart) {
        FussballSpiel fussballSpiel = new FussballSpiel();

        int anzahlFussballTeams = this.fussballTeams.size();

        FussballTeam team1 = this.fussballTeams.get(new Random().nextInt(anzahlFussballTeams));
        FussballTeam team2 = null;

        while (team2 == null || team1 == team2) {
            team2 = this.fussballTeams.get(new Random().nextInt(anzahlFussballTeams));
        }

        fussballSpiel.setHeimTeam(team1);
        fussballSpiel.setGastTeam(team2);

        setQuoteForFussballSpiel(fussballSpiel);

        fussballSpiel.setSpielStart(spielstart);
        fussballSpiel.setSpielEnde(DateUtility.addMinutesToDate(spielstart, 90));

        return fussballSpiel;
    }

    private void setQuoteForFussballSpiel(FussballSpiel fussballSpiel) {
        FussballTeam team1 = fussballSpiel.getHeimTeam();
        FussballTeam team2 = fussballSpiel.getGastTeam();

        int powerDiff = team1.getPowerFaktor() - team2.getPowerFaktor();
        double quoteTeam1 = 0.0;
        double quoteTeam2 = 0.0;

        if (powerDiff <= -90) {
            quoteTeam1 = 200;
            quoteTeam2 = 1.0;
        } else if (powerDiff <= -80) {
            quoteTeam1 = 160;
            quoteTeam2 = 1.0;
        } else if (powerDiff <= -70) {
            quoteTeam1 = 120;
            quoteTeam2 = 1.0;
        } else if (powerDiff <= -60) {
            quoteTeam1 = 80;
            quoteTeam2 = 1.0;
        } else if (powerDiff <= -50) {
            quoteTeam1 = 40;
            quoteTeam2 = 1.01;
        } else if (powerDiff <= -40) {
            quoteTeam1 = 30;
            quoteTeam2 = 1.1;
        } else if (powerDiff <= -30) {
            quoteTeam1 = 14;
            quoteTeam2 = 1.2;
        } else if (powerDiff <= -20) {
            quoteTeam1 = 9;
            quoteTeam2 = 1.5;
        } else if (powerDiff <= -10) {
            quoteTeam1 = 5.5;
            quoteTeam2 = 1.8;
        } else if (powerDiff <= 0) {
            double quoteBeide = (double) (new Random().nextInt((28 - 20) + 1) + 20) / 10;
            quoteTeam1 = quoteBeide;
            quoteTeam2 = quoteBeide;
        } else if (powerDiff <= 10) {
            double quoteBeide = (double) (new Random().nextInt((28 - 20) + 1) + 20) / 10;
            quoteTeam1 = quoteBeide;
            quoteTeam2 = quoteBeide;
        } else if (powerDiff <= 20) {
            quoteTeam1 = 1.9;
            quoteTeam2 = 5.5;
        } else if (powerDiff <= 30) {
            quoteTeam1 = 1.6;
            quoteTeam2 = 9;
        } else if (powerDiff <= 40) {
            quoteTeam1 = 1.25;
            quoteTeam2 = 14;
        } else if (powerDiff <= 50) {
            quoteTeam1 = 1.1;
            quoteTeam2 = 30;
        } else if (powerDiff <= 60) {
            quoteTeam1 = 1.01;
            quoteTeam2 = 40;
        } else if (powerDiff <= 70) {
            quoteTeam1 = 1.0;
            quoteTeam2 = 80;
        } else if (powerDiff <= 80) {
            quoteTeam1 = 1.0;
            quoteTeam2 = 120;
        } else if (powerDiff <= 90) {
            quoteTeam1 = 1.0;
            quoteTeam2 = 160;
        } else if (powerDiff <= 100) {
            quoteTeam1 = 1.0;
            quoteTeam2 = 200;
        }

        fussballSpiel.setQuoteHeim(quoteTeam1);
        fussballSpiel.setQuoteGast(quoteTeam2);
    }
}
