package de.dsa.spw.model;

import java.util.ArrayList;
import java.util.List;

public class Wettschein {

    private int id;
    private int nummer;
    private List<FussballWette> fussballwetten;
    private double einsatz;
    private double moeglicherGewinn;

    public Wettschein() {
    }

    public Wettschein(int nummer, List<FussballWette> fussballwetten, double einsatz, double moeglicherGewinn) {
        this.nummer = nummer;
        this.fussballwetten = fussballwetten;
        this.einsatz = einsatz;
        this.moeglicherGewinn = moeglicherGewinn;
    }

    public void addFussballWette(FussballWette wette){
        this.fussballwetten.add(wette);
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public List<FussballWette> getFussballwetten() {
        return fussballwetten;
    }

    public void setFussballwetten(List<FussballWette> fussballwetten) {
        this.fussballwetten = fussballwetten;
    }

    public double getEinsatz() {
        return einsatz;
    }

    public void setEinsatz(double einsatz) {
        this.einsatz = einsatz;
    }

    public double getMoeglicherGewinn() {
        return moeglicherGewinn;
    }

    public void setMoeglicherGewinn(double moeglicherGewinn) {
        this.moeglicherGewinn = moeglicherGewinn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
