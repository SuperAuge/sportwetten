package de.dsa.spw.model.rest;

import de.dsa.spw.model.FussballWette;
import de.dsa.spw.model.Wettschein;

public class GOWetteWettschein {

    private FussballWette wette;
    private Wettschein wettschein;

    public GOWetteWettschein() {
    }

    public FussballWette getWette() {
        return wette;
    }

    public void setWette(FussballWette wette) {
        this.wette = wette;
    }

    public Wettschein getWettschein() {
        return wettschein;
    }

    public void setWettschein(Wettschein wettschein) {
        this.wettschein = wettschein;
    }
}
