package de.dsa.spw.model;

import java.util.Date;
import java.util.List;

public class User {

    private int UserId;
    private String benutzername;
    private String passwort;
    private double guthaben;
    private List<Wettschein> wettscheine;
    private Date registrationDate;
    private Date lastLogin;

    public User() {
    }

    public User(String benutzername, String passwort, double guthaben) {
        this.benutzername = benutzername;
        this.passwort = passwort;
        this.guthaben = guthaben;
    }

    public User(String benutzername, String passwort, double guthaben, List<Wettschein> wettscheine) {
        this.benutzername = benutzername;
        this.passwort = passwort;
        this.guthaben = guthaben;
        this.wettscheine = wettscheine;
    }

    public String getBenutzername() {
        return benutzername;
    }

    public void setBenutzername(String benutzername) {
        this.benutzername = benutzername;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public double getGuthaben() {
        return guthaben;
    }

    public void setGuthaben(double guthaben) {
        this.guthaben = guthaben;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public List<Wettschein> getWettscheine() {
        return wettscheine;
    }

    public void setWettscheine(List<Wettschein> wettscheine) {
        this.wettscheine = wettscheine;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }
}
