package de.dsa.spw.model;

public class FussballWette {

    private int id;
    private FussballSpiel spiel;
    private Wetteingabe tipp;

    public FussballWette() {
    }

    public FussballWette(FussballSpiel spiel, Wetteingabe tipp) {
        this.spiel = spiel;
        this.tipp = tipp;
    }

    public FussballSpiel getSpiel() {
        return spiel;
    }

    public void setSpiel(FussballSpiel spiel) {
        this.spiel = spiel;
    }

    public Wetteingabe getTipp() {
        return tipp;
    }

    public void setTipp(Wetteingabe tipp) {
        this.tipp = tipp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
