package de.dsa.spw.model;

public class FussballTeam {

    private int id;
    private String name;
    private String liga;
    // PowerFaktor Zahl von 1-100 je nachdem wie stark das Team ist..
    // anhand dieser Zahl wird am Ende die Quote errechnet
    // z.B. Bayern = 95, Gladbach = 80, 1.FC Köln = 30
    private int powerFaktor;

    public FussballTeam() {
    }

    public FussballTeam(String name, String liga, int powerFaktor) {
        this.name = name;
        this.liga = liga;
        this.powerFaktor = powerFaktor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLiga() {
        return liga;
    }

    public void setLiga(String liga) {
        this.liga = liga;
    }

    public int getPowerFaktor() {
        return powerFaktor;
    }

    public void setPowerFaktor(int powerFaktor) {
        this.powerFaktor = powerFaktor;
    }

    @Override
    public String toString() {
        return "FussballTeam{" +
                "name='" + name + '\'' +
                ", liga='" + liga + '\'' +
                ", powerFaktor=" + powerFaktor +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
