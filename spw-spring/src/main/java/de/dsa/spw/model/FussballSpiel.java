package de.dsa.spw.model;

import java.util.Date;

public class FussballSpiel {

    private int id;
    private Date spielStart;
    private Date spielEnde;
    private FussballTeam heimTeam;
    private FussballTeam gastTeam;
    private int toreHeim;
    private int toreGast;
    private double quoteHeim;
    private double quoteGast;
    private double quoteX;

    public FussballSpiel() {
    }

    public FussballSpiel(Date spielStart, Date spielEnde, FussballTeam heimTeam, FussballTeam gastTeam,
                         int toreHeim, int toreGast, double quoteHeim, double quoteGast, double quoteX) {
        this.spielStart = spielStart;
        this.spielEnde = spielEnde;
        this.heimTeam = heimTeam;
        this.gastTeam = gastTeam;
        this.toreHeim = toreHeim;
        this.toreGast = toreGast;
        this.quoteHeim = quoteHeim;
        this.quoteGast = quoteGast;
    }

    public Date getSpielStart() {
        return spielStart;
    }

    public void setSpielStart(Date spielStart) {
        this.spielStart = spielStart;
    }

    public Date getSpielEnde() {
        return spielEnde;
    }

    public void setSpielEnde(Date spielEnde) {
        this.spielEnde = spielEnde;
    }

    public FussballTeam getHeimTeam() {
        return heimTeam;
    }

    public void setHeimTeam(FussballTeam heimTeam) {
        this.heimTeam = heimTeam;
    }

    public FussballTeam getGastTeam() {
        return gastTeam;
    }

    public void setGastTeam(FussballTeam gastTeam) {
        this.gastTeam = gastTeam;
    }

    public int getToreHeim() {
        return toreHeim;
    }

    public void setToreHeim(int toreHeim) {
        this.toreHeim = toreHeim;
    }

    public int getToreGast() {
        return toreGast;
    }

    public void setToreGast(int toreGast) {
        this.toreGast = toreGast;
    }

    public double getQuoteHeim() {
        return quoteHeim;
    }

    public void setQuoteHeim(double quoteHeim) {
        this.quoteHeim = quoteHeim;
    }

    public double getQuoteGast() {
        return quoteGast;
    }

    public void setQuoteGast(double quoteGast) {
        this.quoteGast = quoteGast;
    }

    public double getQuoteX() {
        return quoteX;
    }

    public void setQuoteX(double quoteX) {
        this.quoteX = quoteX;
    }

    @Override
    public String toString() {
        return "FussballSpiel{" +
                "spielStart=" + spielStart +
                ", spielEnde=" + spielEnde +
                ", heimTeam=" + heimTeam +
                ", gastTeam=" + gastTeam +
                ", toreHeim=" + toreHeim +
                ", toreGast=" + toreGast +
                ", quoteHeim=" + quoteHeim +
                ", quoteGast=" + quoteGast +
                ", quoteX=" + quoteX +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
