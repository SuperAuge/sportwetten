package de.dsa.spw.model.rest;

import de.dsa.spw.model.FussballWette;
import de.dsa.spw.model.User;
import de.dsa.spw.model.Wettschein;

public class GOUserWettschein {

    private User user;
    private Wettschein wettschein;

    public GOUserWettschein() {
    }

    public GOUserWettschein(User user, Wettschein wettschein) {
        this.user = user;
        this.wettschein = wettschein;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Wettschein getWettschein() {
        return wettschein;
    }

    public void setWettschein(Wettschein wettschein) {
        this.wettschein = wettschein;
    }
}
