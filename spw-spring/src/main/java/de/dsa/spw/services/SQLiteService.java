package de.dsa.spw.services;

import de.dsa.spw.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class SQLiteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SQLiteService.class);

    private final StatementService statementService = new StatementService();

    private final String url = "jdbc:sqlite:database/sportwetten.db";

    Wettschein test = new Wettschein(500, null, 100, 200);
    FussballSpiel test3 = new FussballSpiel(new Date(), new Date(), new FussballTeam(), new FussballTeam(), 0, 1, 2, 3, 4);


    public SQLiteService() {
        openOrCreateNewDatabase();
        try (Connection conn = DriverManager.getConnection(url)) {
            String db_teamSQL = String.join(" ", Files.readAllLines(Paths.get("database/db_teamSQL")));
            PreparedStatement statement = conn.prepareStatement(db_teamSQL);
            statement.executeUpdate();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        List<FussballWette> test2 = new ArrayList<>();
        test2.add(new FussballWette(test3, Wetteingabe.X));
        test.setFussballwetten(test2);
        addCoupon(2, test);

    }

    /**
     * Öffnet eine Verbindung zur Datenbank oder erstellt diese, wenn noch keine existiert
     */
    public void openOrCreateNewDatabase() {
        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                createTablesIfNotExists();
            }
        } catch (SQLException e) {
            LOGGER.error("Es gab Fehler beim Erstellen einer Datenbank-Connection.");
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Erstellt alle nötigen Tabellen, wenn sie noch nicht existieren
     */
    private void createTablesIfNotExists() {
        LOGGER.debug("execute createTablesIfNotExists()");
        //Try create UserTable
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.createUserTableStatement());
            statement.executeUpdate();
            LOGGER.info("Die Tabelle 'User' wurde erstellt.");

        } catch (SQLException e) {
            if (e.getErrorCode() == 1) {
                // UserTable already exists
                LOGGER.info("Die Tabelle ’User’ existiert bereits.");
            } else {
                LOGGER.error("Es gab Probleme beim Ausführen des Statements.");
                LOGGER.error(e.getMessage(), e);
            }
        }

        //Try create TeamTable
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.createTeamTableStatement());
            statement.executeUpdate();
            LOGGER.info("Die Tabelle 'Team' wurde erstellt.");

        } catch (SQLException e) {
            if (e.getErrorCode() == 1) {
                // TeamTable already exists
                LOGGER.info("Die Tabelle ’Team’ existiert bereits.");
            } else {
                LOGGER.error("Es gab Probleme beim Ausführen des Statements.");
                LOGGER.error(e.getMessage(), e);
            }
        }

        //Try create GameTable
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(StatementService.createGameTableStatement());
            statement.executeUpdate();
            LOGGER.info("Die Tabelle 'Game' wurde erstellt.");

        } catch (SQLException e) {
            if (e.getErrorCode() == 1) {
                // GameTable already exists
                LOGGER.info("Die Tabelle ’Game’ existiert bereits.");
            } else {
                LOGGER.error("Es gab Probleme beim Ausführen des Statements.");
                LOGGER.error(e.getMessage(), e);
            }
        }

        //Try create CouponTable
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.createCouponTableStatement());
            statement.executeUpdate();

        } catch (SQLException e) {
            if (e.getErrorCode() == 1) {
                // CouponTable already exists
                LOGGER.info("Die Tabelle ’Coupon’ existiert bereits.");
            } else {
                LOGGER.error("Es gab Probleme beim Ausführen des Statements.");
                LOGGER.error(e.getMessage(), e);
            }
        }

        //Try create BetTable
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.createBetTableStatement());
            statement.executeUpdate();
            LOGGER.info("Die Tabelle 'Bet' wurde erstellt.");

        } catch (SQLException e) {
            if (e.getErrorCode() == 1) {
                // BetTable already exists
                LOGGER.info("Die Tabelle ’Bet’ existiert bereits.");
            } else {
                LOGGER.error("Es gab Probleme beim Ausführen des Statements.");
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Gibt einen User aus der Datenbank anhand seines Benutzernamens zurück
     *
     * @param username Der Benutzername
     * @return Gibt den User zurück oder null bei einem Fehler oder wenn kein User mit dem Benutzernamen existiert
     */
    public User getUser(String username) {
        LOGGER.debug("execute getUser()");
        User user = new User();
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.getUserStatement(username));
            //findUser
            ResultSet res = statement.executeQuery();

            if (res.next()) {
                try {
                    user.setUserId(res.getInt("UserId"));
                    user.setBenutzername(res.getString("Username"));
                    user.setPasswort(res.getString("Password"));
                    user.setGuthaben(res.getFloat("Balance"));
                    user.setRegistrationDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(res.getString("RegistrationDate")));
                    user.setLastLogin(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(res.getString("LastLogin")));
                    //statement.executeUpdate(setLastLoginStatement(user.getUserId())); TODO
                } catch (Exception e) {
                    LOGGER.error("Es gab Probleme beim Ausführen des Statements.");
                    LOGGER.error(e.getMessage(), e);
                }
            } else {
                //no User found
                user = null;
            }

        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return user;
    }

    /**
     * Fügt einen neuen User zur Datenbank hinzu
     *
     * @param username Der Benutzername
     * @param password Das Passwort
     * @return Gibt den neuen User zurück oder null bei einem Fehler
     */
    public User createUser(String username, String password) {
        User user = new User(username, password, 50);

        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.createUserStatement(username, password));
            statement.executeUpdate();
        } catch (SQLException e) {
            user = null;
        }
        return user;
    }

    /**
     * Fügt einem User Guthaben hinzu.
     *
     * @param user  Der User
     * @param money Der Betrag
     * @return Gibt den User mit dem neuen Kontostand zurück
     */
    public User addMoney(User user, double money) {
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.addMoneyStatement(user.getUserId(), money));
            statement.executeUpdate();
            user.setGuthaben(user.getGuthaben() + money);
            return user;
        } catch (SQLException e) {
            LOGGER.error("Es gab probleme beim Hinzufügen des Guthabens auf das Konto.");
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Entfernt einem User Guthaben.
     *
     * @param userId Der User
     * @param money  Der Betrag
     * @return Gibt true bei Erfolg und false bei einem Fehler zurück
     */
    public boolean removeMoney(int userId, double money) {
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.removeMoneyStatement(userId, money));
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }

    /**
     * Gibt eine Liste aller FussballTeams zurück.
     *
     * @return Die Liste aller FussballTeams in der Datenbank
     */
    public List<FussballTeam> getFussballTeams() {
        List<FussballTeam> teams = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.getTeamsStatement());
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                teams.add(new FussballTeam(
                        resultSet.getString("TeamName"),
                        resultSet.getString("Liga"),
                        resultSet.getInt("PowerFactor")
                ));
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }

        return teams;
    }

    /**
     * Fügt ein Team zur Datenbank hinzu.
     *
     * @param team Das neue Team
     * @return Gibt das neue Team zurück oder null bei einem Fehler
     */
    public FussballTeam addTeam(FussballTeam team) {
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.addTeamStatement(team));
            statement.executeUpdate();
            return team;
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * Fügt ein Fussballspiel zur Datenbank hinzu
     *
     * @param game Das neue Fussballspiel
     * @return Gibt das neue Fussballspiel zurück oder null bei einem Fehler
     */
    public FussballSpiel addGame(FussballSpiel game) {
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statement = conn.prepareStatement(statementService.addGameStatement(game));
            statement.executeUpdate();
            return game;
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * Fügt einen Wettschein zur Datenbank hinzu
     *
     * @param userId Der User dem der Wettschei gehört
     * @param coupon Der neue Wettschein
     * @return Gibt den neuen Wettschein zurück oder null bei einem Fehler
     */
    public Wettschein addCoupon(int userId, Wettschein coupon) {
        try (Connection conn = DriverManager.getConnection(url)) {
            //Wettschein hinzufügen
            PreparedStatement statement = conn.prepareStatement(statementService.addCouponStatement(userId, coupon.getEinsatz()));
            statement.executeUpdate();

            Statement stm = conn.createStatement();
            ResultSet maxId = stm.executeQuery(statementService.getMaxIdInTable("Coupon"));
            coupon.setId(maxId.getInt(1));
            //Schleife fügt jede Fussballwette des Wettscheins zur Datenbank hinzufügen
            for (FussballWette bet : coupon.getFussballwetten()) {
                stm.executeUpdate(statementService.addBetStatement(coupon.getId(), bet.getId(), bet.getTipp().ordinal()));
            }

            return coupon;
        } catch (SQLException e) {
            return null;
        }

    }

    /**
     * Gibt eine Liste von Wettscheinen von einem User zurück
     *
     * @param userId Der User
     * @return Gibt alle Wettscheine des Users zurück oder null bei einem Fehler
     */
    public List<Wettschein> getCouponsFromUser(int userId) {
        ArrayList<FussballTeam> teams = (ArrayList<FussballTeam>) getFussballTeams();
        List<Wettschein> list = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(url)) {
            //Alle Wettscheine
            PreparedStatement statement = conn.prepareStatement(statementService.getCouponsFromUserStatement(userId));
            ResultSet res = statement.executeQuery();

            while (res.next()) {
                list.add(new Wettschein(res.getInt(1), null, res.getDouble(3), 0));
            }

            for (Wettschein coupon : list) {
                //Alle Wette für jeden Wettschein
                Statement stm = conn.createStatement();
                res = stm.executeQuery(statementService.getBetsFromCouponStatement(coupon.getId()));
                while (res.next()) {
                    //coupon.addFussballWette(new FussballWette(res.getInt(3), res.getInt(4)));
                }
            }

            //TODO

            return list;
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * Gibt eine Liste von Fussballwetten eines Wettscheins zurück
     *
     * @param couponId Der Wettschein
     * @return Gibt alle Fussballwetten eines Wettscheins zurück oder null bei einem Fehler
     */
    public List<FussballWette> getBetsFromCoupon(int couponId) {
        //alle Wetten von einem Wettschein
        return null;
    }

    public Wettschein addWettscheinToUser(User user, Wettschein wettschein, FussballWette fussballWette) {

        Wettschein aktualisierterWettschein = null;

        try (Connection connection = DriverManager.getConnection(url)) {
            PreparedStatement statement = connection.prepareStatement(statementService.addCouponStatement(user.getUserId(), wettschein.getEinsatz()));
            statement.execute();

            PreparedStatement statement2 = connection.prepareStatement(statementService.getCouponsFromUserStatement(user.getUserId()));
            ResultSet resultSet = statement2.executeQuery();

            while (resultSet.next()) {
                aktualisierterWettschein = new Wettschein(resultSet.getInt("CouponID"), Collections.singletonList(fussballWette),
                        resultSet.getDouble("BetAmount"), wettschein.getMoeglicherGewinn());
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return aktualisierterWettschein;
    }

    public boolean addFussballWetteToWettschein(FussballWette wette, Wettschein wettschein) {
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement statementBet = conn.prepareStatement(
                    statementService.addBetStatement(wettschein.getId(), wette.getSpiel().getId(), wette.getTipp().ordinal()));
            statementBet.execute();

//            PreparedStatement statement = conn.prepareStatement(statementService.addFussballWetteToWettscheinStatement(wette, wettschein));
//            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Es gab Fehler beim Ausführen von addFussballWetteToWettschein()");
            LOGGER.error(e.getMessage(), e);
            return false;
        }

        return true;
    }
}
