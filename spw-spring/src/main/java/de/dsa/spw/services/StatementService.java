package de.dsa.spw.services;

import de.dsa.spw.model.FussballSpiel;
import de.dsa.spw.model.FussballTeam;
import de.dsa.spw.model.FussballWette;
import de.dsa.spw.model.Wettschein;

public class StatementService {

    public String addCouponStatement(int userId, double betAmount){
        return "INSERT INTO Coupon " +
                "(UserID, BetAmount) " +
                "VALUES (" + userId + "," + betAmount + ");";
    }

    public String getCouponsFromUserStatement(int userId){
        return "SELECT * FROM Coupon " +
                "WHERE UserID = " + userId + ";";
    }

    public String getBetsFromCouponStatement(int couponId){
        return "SELECT * FROM Bet " +
                "WHERE CouponID = " + couponId + ";";
    }

    public String addBetStatement(int couponId, int gameId, int bet){
        return "INSERT INTO Bet " +
                "(CouponID, GameID, Bet) " +
                "VALUES ( " + couponId + "," + gameId + "," + bet + ");";
    }

    public String getMaxIdInTable(String tableName){
        return "SELECT MAX(" + tableName + "ID) FROM " + tableName + ";";
    }

    //TODO
    public String addGameStatement(FussballSpiel game) {
        return "INSERT INTO Game " +
                "(StartTime, EndTime, Team1, Team2, QuoteTeam1, QuoteTeam2, QuoteDifference, GoalsTeam1, GoalsTeam2) " +
                "VALUES ();";
    }

    public String addTeamStatement(FussballTeam team) {
        return "INSERT INTO Team " +
                "(TeamName, PowerFactor) " +
                "VALUES ('" + team.getName() + "'," + team.getPowerFaktor() + ");";
    }

    public String removeMoneyStatement(int userId, double amount) {
        return "UPDATE User " +
                "SET Balance = Balance - " + amount + " " +
                "WHERE UserID = " + userId;
    }

    public String addMoneyStatement(int userId, double amount) {
        return "UPDATE User " +
                "SET Balance = Balance + " + amount + " " +
                "WHERE UserID = " + userId;
    }

    public String getUserStatement(String username) {
        return "SELECT * " +
                "FROM User " +
                "WHERE Username = '" + username + "'";
    }

    public String getTeamsStatement() {
        return "SELECT * FROM Team";
    }

    public String setLastLoginStatement(int userId) {
        return "UPDATE User " +
                "SET LastLogin = CURRENT_TIMESTAMP " +
                "WHERE UserId = " + userId;
    }

    public String createUserStatement(String username, String password) {
        return "INSERT INTO User (Username, Password, Balance) " +
                "VALUES ( '" + username + "','" + password + "',50);";
    }

    public String createUserTableStatement() {
        return "CREATE TABLE User (" +
                "UserID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "Username varchar(255) NOT NULL, " +
                "Password varchar(255) NOT NULL, " +
                "Balance float, " +
                "RegistrationDate Text DEFAULT CURRENT_TIMESTAMP, " +
                "LastLogin Text DEFAULT CURRENT_TIMESTAMP" +
                ");";
    }

    public static String createGameTableStatement() {
        return "CREATE TABLE Game (" +
                "SpielID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "StartTime Text, " +
                "EndTime Text, " +
                "Team1 int, " +
                "Team2 int, " +
                "QuoteTeam1 float, " +
                "QuoteTeam2 float, " +
                "QuoteDifference float, " +
                "GoalsTeam1 int, " +
                "GoalsTeam2 int, " +
                "FOREIGN KEY (Team1) REFERENCES Team(TeamID), " +
                "FOREIGN KEY (Team2) REFERENCES Team(TeamID) " +
                ");";
    }

    public String createTeamTableStatement() {
        return "CREATE TABLE Team (" +
                "TeamID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "Liga varchar(255), " +
                "TeamName varchar(255), " +
                "PowerFactor int" +
                ");";
    }

    public String createCouponTableStatement() {
        return "CREATE TABLE Coupon (" +
                "CouponID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "UserID int, " +
                "BetAmount float, " +
                "FOREIGN KEY (UserID) REFERENCES User(UserID) " +
                ");";
    }

    public String createBetTableStatement() {
        return "CREATE TABLE Bet (" +
                "BetID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "CouponID int, " +
                "GameID int, " +
                "Bet int, " +
                "FOREIGN KEY (CouponID) REFERENCES Coupon(CouponID), " +
                "FOREIGN KEY (GameID) REFERENCES Game(GameID) " +
                ");";
    }

    public String addFussballWetteToWettscheinStatement(FussballWette wette, Wettschein wettschein) {
        // TODO
        return "";
    }

}
