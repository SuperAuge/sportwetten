package de.dsa.spw.services;

import de.dsa.spw.controller.FussballWettController;
import de.dsa.spw.model.*;
import de.dsa.spw.model.rest.GOUserWettschein;
import de.dsa.spw.model.rest.GOWetteWettschein;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/rest")
public class RestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestService.class);
    private final SQLiteService sqLiteService = new SQLiteService();
    private FussballWettController fussballWettController = new FussballWettController();

    @RequestMapping(value = "/checklogindata", method = RequestMethod.POST)
    public User checkLoginData(@RequestBody User user) {
        LOGGER.debug("POST /rest/checklogindata");

        return sqLiteService.getUser(user.getBenutzername());
    }

    /**
     * Überprüft ob bereits ein Benutzer unter dem Benutzernamen existiert. Wenn nicht, fügt er diesen mit dem
     * gesetzten Passwort aus dem Frontend in die Datenbank hinzu.
     *
     * @param user Der übergebene zu registrierende User
     * @return statuscode: 0 = erfolgreich, 1 = fehler, 2 = Benutzername existiert bereits
     */
    @RequestMapping(value = "/registriereuser", method = RequestMethod.PUT)
    public int registriereUser(@RequestBody User user) {
        LOGGER.debug("PUT /rest/registriereuser");
        // Existiert der Benutzer bereits?
        if (sqLiteService.getUser(user.getBenutzername()) != null) {
            LOGGER.info("Der Benutzer '{}' existiert bereits.", user.getBenutzername());
            return 2;
        }
        User u = sqLiteService.createUser(user.getBenutzername(), user.getPasswort());

        return u != null ? 0 : 1;
    }

    @RequestMapping(value = "/einzahlen/{betrag}", method = RequestMethod.PUT)
    public User geldEinzahlen(@PathVariable double betrag, @RequestBody User user) {
        return sqLiteService.addMoney(user, betrag);
    }

    @RequestMapping(value = "/zufallsspiel", method = RequestMethod.GET)
    public FussballSpiel getZufallsFussballSpiel() {
        return fussballWettController.createRandomFussballSpiel(new Date());
    }

    @RequestMapping(value = "/addWettscheinToUser", method = RequestMethod.PUT)
    public Wettschein addWettscheinToUser(@RequestBody GOUserWettschein goUserWettschein) {
        User user = goUserWettschein.getUser();
        Wettschein wettschein = goUserWettschein.getWettschein();

        return sqLiteService.addWettscheinToUser(user, wettschein, wettschein.getFussballwetten().get(0));
    }

    @RequestMapping(value = "/addFussballWetteToWettschein", method = RequestMethod.PUT)
    public boolean addFussballWetteToWettschein(@RequestBody GOWetteWettschein generationObject) {
        FussballWette wette = generationObject.getWette();
        Wettschein wettschein = generationObject.getWettschein();

        return sqLiteService.addFussballWetteToWettschein(wette, wettschein);
    }
}
