package de.dsa.spw.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtility {

    public static Date addMinutesToDate(Date date, int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutes);
        return calendar.getTime();
    }
}
